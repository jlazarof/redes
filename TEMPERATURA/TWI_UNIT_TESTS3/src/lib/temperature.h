/*
 * temperature.h
 *
 * Created: 29/10/2021 13:12:30
 * Author: Javier L�zaro Fern�ndez & Sa�l Vikran L�pez Hidalgo
 */ 
#include <stdlib.h>
#include <stdint.h>

/************************************************************************/
/* Definiciones para el Two Wire Interface								*/
/************************************************************************/
#define TWI_MASTER				&TWBR
#define TWI_SPEED				125000
#define TWI_SLAVE_ADDR			0x96
#define SLAVE_MEM_ADDR			0x01
#define SLAVE_MEM_ADDR_LENGTH   TWI_SLAVE_ONE_BYTE_SIZE
#define DATA_LENGTH  sizeof(conf_data)

/************************************************************************/
/* Funciones                                                            */
/************************************************************************/
void sensor_conf (void); // Configuraci�n del sensor de temperatura
uint8_t* read_temperature (void); // Leer el dato de la temperatura
uint16_t adc_meastemp (void); // Leer temperatura procesador
void twi_init (void); // Inicializar el Two Wire Interface
