// Se incluyen las bibliotecas necesarias
#include <asf.h>
#include <twi_megarf.h>
#include <ioport.h>
#include <avr/io.h>
#include <util/delay.h>
#include <lib/temperature.h>	
#include <conf_usart_serial.h>
#include <string.h>


/**************************************************************************/
/* Variables para el temporizador										  */
/**************************************************************************/

unsigned long long time_Elapsed = 0;
unsigned long long count = 0;
bool FLAG_seconds = false;
#define MAX_TIME_OVF 18446744073709551614

#define TEMPERATURE_TIME 1 //seconds

void init_timer(void){
	
	TIMSK2 = 1 << TOIE2; // Enable TC2 overflow interrupt
	TCCR2B = 0x05; // Divide by 128 PAG 359
	
	/**************************************************************************/
	/* f = fclk / (N x 510) -> N = 128, fclk = 16 MHz -> f = 245.09 -> f = 245*/
	/**************************************************************************/
	
	TIFR2=TOV2; // Interrupci�n por comparaci�n
	TCNT2 = 0;
	sei(); // Activar interruppciones
}

/**************************************************************************/
/* Interrupci�n por overflow											  */
/**************************************************************************/

ISR(TIMER2_OVF_vect){ //TIMER2_OVF PG243
	
	count += 1; // Actualizar contador del temporizador
	
	if (count >= 245){ // 1 second
		
		if(time_Elapsed == MAX_TIME_OVF){
		
			time_Elapsed = 0;
			
		} else{
		
			time_Elapsed++;
		
		}
	
	}
	
	if(count >= (245 * TEMPERATURE_TIME)){ //si sumo 1 cada 1/245 ms, para 1s necesito

		LED_Toggle(LED0);
		
		count = 0;
		//printf("\nAntes: %d\n",FLAG_seconds);
		//FLAG_seconds = true;
		//printf("\nDesp: %d\n",FLAG_seconds);
		
	}
	
}

int main(void){
	
	const usart_serial_options_t usart_serial_options = {
		.baudrate   = USART_SERIAL_BAUDRATE,
		.charlength = USART_SERIAL_CHAR_LENGTH,
		.paritytype = USART_SERIAL_PARITY,
		.stopbits   = USART_SERIAL_STOP_BIT,
	};
	
	board_init();

	ioport_init();
	stdio_serial_init(USART_SERIAL, &usart_serial_options);
	
	twi_init();
	sensor_conf();
	
	init_timer();
	
	printf ("\n =================================================== \n \r");
	printf("\n\n LECTURA TEMPERATURA SENSOR EXTERNO\n\n \r");
	printf ("\n =================================================== \n \r");
	
	uint8_t* pData;
	uint8_t msg[256];
	uint8_t msg2[256];
	
	uint16_t temperatura_procesador; // Lo que devuelve el ADC

	float grados_Celsius; // Para la conversi�n de la temperatura anterior a �C

	// Parte entera y decimal de la temperatura del uC
	uint16_t parteEntera;
	uint16_t parteDecimal;
	
	while(true) {
					
		pData = read_temperature(); // Leer temperatura sensor
		temperatura_procesador = adc_meastemp(); // Leer temperatura procesador
		grados_Celsius = (1.13 * temperatura_procesador) - 272.8; // Conversi�n a �C
		parteEntera = (uint16_t)grados_Celsius; // Extraer parte entera
		parteDecimal = (uint16_t)((grados_Celsius - parteEntera)*1000); // Extraer parte decimal
			
		sprintf(msg,"%c%c\n%d%s%02d,%03d\n\r%c%c", 0x10, 0x02, 12, "T_", pData[0], pData[1], 0x03, 0x10);
		sprintf(msg2,"%c%c\n%d%s%02d,%03d\n\r%c%c", 0x10, 0x02, 12, "T_", parteEntera, parteDecimal, 0x03, 0x10);
		printf("[INFO] main | Sending msg [Sensor Temp]: %s\n", msg);
		printf("[INFO] main | Sending msg2 [uC Temp]: %s\n", msg2);
		
		strcpy(msg,""); // Borrar el contenido de msg
		strcpy(msg2,""); // Borrar el contenido de msg
		
		delay_s(1);
		
	
	}
	
	return 0;

}

