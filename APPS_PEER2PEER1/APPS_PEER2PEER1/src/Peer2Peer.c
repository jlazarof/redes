/**
 * \file Peer2Peer.c
 *
 * \brief Peer2Peer application implementation
 *
 * Copyright (c) 2014-2018 Microchip Technology Inc. and its subsidiaries.
 *
 * \asf_license_start
 *
 * \page License
 *
 * Subject to your compliance with these terms, you may use Microchip
 * software and any derivatives exclusively with Microchip products.
 * It is your responsibility to comply with third party license terms applicable
 * to your use of third party software (including open source software) that
 * may accompany Microchip software.
 *
 * THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES,
 * WHETHER EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE,
 * INCLUDING ANY IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY,
 * AND FITNESS FOR A PARTICULAR PURPOSE. IN NO EVENT WILL MICROCHIP BE
 * LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE, INCIDENTAL OR CONSEQUENTIAL
 * LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND WHATSOEVER RELATED TO THE
 * SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP HAS BEEN ADVISED OF THE
 * POSSIBILITY OR THE DAMAGES ARE FORESEEABLE.  TO THE FULLEST EXTENT
 * ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS IN ANY WAY
 * RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT OF FEES, IF ANY,
 * THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.
 *
 * \asf_license_stop
 *
 *
 */

/**
 * \mainpage
 * \section preface Preface
 * This is the reference manual for the LWMesh Peer2Peer Application
 * The LightWeight Mesh Peer2Peer  implements a wireless UART application.Two nodes are used in this application
 * These two nodes must be configured with addresses 0x0001 and 0x0000 respectively.
 * To test this application,open a terminal for both the nodes.On entering text in the terminal the data is transmitted from one 
 * node to another node(0x0001 to 0x0000 and vice-versa)
 */
/*- Includes ---------------------------------------------------------------*/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "config.h"
#include "sys.h"
#if SAMD || SAMR21 || SAML21
#include "system.h"
#else
#include "led.h"
#include "sysclk.h"
#endif
#include "phy.h"
#include "nwk.h"
#include "sysTimer.h"
#include "sio2host.h"
#include "asf.h"

#include "avr/interrupt.h"
#include <twi_megarf.h>

bool FLAG_seconds = false;
#define TEMPERATURE_TIME 20 //seconds
#define NODE_ID			 12

#define ALARMA_ON		"A_1"
#define ALARMA_OFF		"A_0"
#define ALARM_N1		"W"
#define GET_TEMP		"R_"
#define ACK				"K"
#define TEMP			"T_"
unsigned long long time_Elapsed = 0;


const uint8_t conf_data[] = {
	0x60
};

typedef struct temp_ctx_t{
	uint8_t readData[2];
} TEMP_CTX_T;


void sensor_conf (void);
uint8_t* read_temperature (void);
void twi_init (void);
uint16_t adc_meastemp (void);

TEMP_CTX_T temp_ctx;

// Definimos la variable de la temperatura sacada del sensor externo
uint8_t *pData;

#define TWI_MASTER				&TWBR

#define TWI_SPEED				125000

#define TWI_SLAVE_ADDR			0x96

#define SLAVE_MEM_ADDR			0x01

#define SLAVE_MEM_ADDR_LENGTH   TWI_SLAVE_ONE_BYTE_SIZE

#define DATA_LENGTH  sizeof(conf_data)



void sensor_conf (void){
	/* configures the TWI configuration packet*/
	twi_package_t packet = {
		.addr[0] = (uint8_t) SLAVE_MEM_ADDR,
		.addr_length = (uint8_t)SLAVE_MEM_ADDR_LENGTH,
		.chip = TWI_SLAVE_ADDR,
		.buffer = (void *)conf_data,
		.length = DATA_LENGTH
	};
	/* Perform a multi-byte write access */
	while (twi_master_write(TWI_MASTER,&packet) != TWI_SUCCESS) {
	}
	/* waits for write completion*/
	delay_ms(5);
}


// FUNCI?N PARA LEER LA TEMPERATURA DEL SENSOR EXTERNO
uint8_t* read_temperature (void){
	uint8_t received_data[2] = {0, 0};
	
	
	/* configures the TWI read packet*/
	twi_package_t packet_received = {
		.addr[0] = 0x00,
		.addr_length = (uint8_t)SLAVE_MEM_ADDR_LENGTH,
		.chip = TWI_SLAVE_ADDR,
		.buffer = received_data,
		.length = 2,
	};
	/* Perform a multi-byte read access*/
	while (twi_master_read(TWI_MASTER,&packet_received) != TWI_SUCCESS) {
	}
	temp_ctx.readData[0] = received_data[0];
	temp_ctx.readData[1] = received_data[1];
	
	// SE PUEDE IMPRIMIR POR EL HIPERTERMINAL AQU?, PERO LO HAREMOS EN EL MAIN
	return temp_ctx.readData;
}



void twi_init (void){
	/* TWI master initialization options. */
	twi_master_options_t m_options = {
		.speed      = TWI_SPEED,
		.chip  = TWI_SLAVE_ADDR,
	};
	m_options.baud_reg = TWI_CLOCK_RATE(sysclk_get_cpu_hz(), m_options.speed);
	/* Enable the peripheral clock for TWI module */
	sysclk_enable_peripheral_clock(TWI_MASTER);
	/* Initialize the TWI master driver. */
	twi_master_init(TWI_MASTER,&m_options);
}

/*************************************************************************//**
*****************************************************************************/

/*- Definitions ------------------------------------------------------------*/
#ifdef NWK_ENABLE_SECURITY
  #define APP_BUFFER_SIZE     (NWK_MAX_PAYLOAD_SIZE - NWK_SECURITY_MIC_SIZE)
#else
  #define APP_BUFFER_SIZE     NWK_MAX_PAYLOAD_SIZE
#endif

static uint8_t rx_data[APP_RX_BUF_SIZE];

/*- Types ------------------------------------------------------------------*/
typedef enum AppState_t {
	APP_STATE_INITIAL,
	APP_STATE_IDLE,
} AppState_t;

/*- Prototypes -------------------------------------------------------------*/
static void appSendData(void);

/*- Variables --------------------------------------------------------------*/
static AppState_t appState = APP_STATE_INITIAL;
static SYS_Timer_t appTimer;
static NWK_DataReq_t appDataReq;
static bool appDataReqBusy = false;
static uint8_t appDataReqBuffer[APP_BUFFER_SIZE];
static uint8_t appUartBuffer[APP_BUFFER_SIZE];
static uint8_t appUartBufferPtr = 0;
static uint8_t sio_rx_length;
/*- Implementations --------------------------------------------------------*/

/*************************************************************************//**
*****************************************************************************/
static void appDataConf(NWK_DataReq_t *req)
{
	appDataReqBusy = false;
	(void)req;
}

/*************************************************************************//**
*****************************************************************************/
static void appSendData(void)
{
	if (appDataReqBusy) {
		return;
	}
	LED_On(LED0);
	//memcpy(appDataReqBuffer, appUartBuffer, appUartBufferPtr);
	pData = read_temperature();
	sprintf(appDataReqBuffer,"%c%c\n%d%s%02d,%03d\n\r%c%c", 0x10, 0x02, NODE_ID, TEMP, pData[0],pData[1], 0x03, 0x10);
	printf("[INFO] appSendData | Sending msg: %s\n",appDataReqBuffer);
	appDataReq.dstAddr = 1 - APP_ADDR;
	appDataReq.dstEndpoint = APP_ENDPOINT;
	appDataReq.srcEndpoint = APP_ENDPOINT;
	appDataReq.options = NWK_OPT_ENABLE_SECURITY;
	appDataReq.data = appDataReqBuffer;
	appDataReq.size = sizeof(appDataReqBuffer);
	appDataReq.confirm = appDataConf;
	NWK_DataReq(&appDataReq);

	appUartBufferPtr = 0;
	appDataReqBusy = true;
	LED_Off(LED0);
}

/*************************************************************************//**
*****************************************************************************/
static void appTimerHandler(SYS_Timer_t *timer)
{
	appSendData();
	(void)timer;
}

/*************************************************************************//**
*****************************************************************************/
static bool appDataInd(NWK_DataInd_t *ind)
{
	for (uint8_t i = 0; i < ind->size; i++) {
		sio2host_putchar(ind->data[i]);
	}
	LED_Toggle(LED0);
	return true;
}

/*************************************************************************//**
*****************************************************************************/
static void appInit(void)
{
	NWK_SetAddr(APP_ADDR);
	NWK_SetPanId(APP_PANID);
	PHY_SetChannel(APP_CHANNEL);

	PHY_SetRxState(true);

	NWK_OpenEndpoint(APP_ENDPOINT, appDataInd);

	appTimer.interval = APP_FLUSH_TIMER_INTERVAL;
	appTimer.mode = SYS_TIMER_INTERVAL_MODE;
	appTimer.handler = appTimerHandler;
}

/*************************************************************************//**
*****************************************************************************/
static void APP_TaskHandler(void)
{
	switch (appState) {
	case APP_STATE_INITIAL:
	{
		appInit();
		appState = APP_STATE_IDLE;
	}
	break;

	case APP_STATE_IDLE:
		break;

	default:
		break;
	}
	
	sio_rx_length = sio2host_rx(rx_data, APP_RX_BUF_SIZE);
	
	if (sio_rx_length) {
		for (uint16_t i = 0; i < sio_rx_length; i++) {
			sio2host_putchar(rx_data[i]);
			if (appUartBufferPtr == sizeof(appUartBuffer)) {
				appSendData();
			}

			if (appUartBufferPtr < sizeof(appUartBuffer)) {
				appUartBuffer[appUartBufferPtr++] = rx_data[i];
			}
		}

		SYS_TimerStop(&appTimer);
		SYS_TimerStart(&appTimer);
	}
	if(ioport_pin_is_low(GPIO_PUSH_BUTTON_0) || FLAG_seconds){
		FLAG_seconds = false; // Clear flag
		appSendData();
		SYS_TimerStop(&appTimer);
		SYS_TimerStart(&appTimer);	
	}	
	
	/* ALARMA RECIBIDA */
	
	if(!strcmp(rx_data,ALARMA_ON)){
		printf("ALARMA_ON");
	}
}

/*************************************************************************//**
*****************************************************************************/

unsigned long long count = 0;
void init_timer(void){
	TIMSK2 = 1 << TOIE2; // Enable TC2 overflow interrupt
	TCCR2B = 0x05; //Divide by 128 PAG 359 
	/**************************************************************************/
	/* f = fclk / (N x 510) -> N = 128, fclk = 16 MHz -> f = 245.09 -> f = 245*/
	/**************************************************************************/
	TIFR2=TOV2; // INTERRUPCION CUANDO COMPARE
	TCNT2 = 0;  
	sei(); //ACTIVAR INTERRUPCIONES
}

ISR(TIMER2_OVF_vect){ //TIMER2_OVF PG243
	count+=1;
	if (count >= 245){ // 1 second
		if(time_Elapsed == 9223372036854775807)
			time_Elapsed = 0;
		else
			time_Elapsed++;
	}
	if(count >= (245 * TEMPERATURE_TIME)){ //si sumo 1 cada 1/245 ms, para 1s necesito 
		count=0;
		//LED_Toggle(LED0);
		FLAG_seconds = true;
	}
}

int main(void)
{
	
	irq_initialize_vectors();
	//sysclk_init();
	board_init();
	
	SYS_Init();
	sio2host_init();
	cpu_irq_enable();
	//LED_On(LED0);
	
	ioport_init();
	
	twi_init();
	sensor_conf();	
	init_timer();
	
	while (1) {
		SYS_TaskHandler();
		APP_TaskHandler();
	}
	
}
